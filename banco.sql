/*
SQLyog Community v13.0.1 (64 bit)
MySQL - 10.2.14-MariaDB : Database - cases
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`cases` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `cases`;

/*Table structure for table `attachments` */

DROP TABLE IF EXISTS `attachments`;

CREATE TABLE `attachments` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `occurrence_id` int(10) unsigned NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `attachments_occurrence_id_foreign` (`occurrence_id`),
  CONSTRAINT `attachments_occurrence_id_foreign` FOREIGN KEY (`occurrence_id`) REFERENCES `occurrences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `attachments` */

/*Table structure for table `messages` */

DROP TABLE IF EXISTS `messages`;

CREATE TABLE `messages` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `occurrence_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `messages_occurrence_id_foreign` (`occurrence_id`),
  KEY `messages_user_id_foreign` (`user_id`),
  CONSTRAINT `messages_occurrence_id_foreign` FOREIGN KEY (`occurrence_id`) REFERENCES `occurrences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `messages_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `messages` */

/*Table structure for table `migrations` */

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `migrations` */

insert  into `migrations`(`id`,`migration`,`batch`) values 
(1,'2009_04_22_172414_create_roles_table',1),
(2,'2014_10_12_000000_create_users_table',1),
(3,'2014_10_12_100000_create_password_resets_table',1),
(4,'2019_04_22_182309_create_situations_table',1),
(5,'2019_04_22_191423_create_occurrences_table',1),
(6,'2019_04_23_172155_create_messages_table',1),
(7,'2019_04_26_160309_create_notifications_table',1),
(8,'2019_04_27_191550_create_attachments_table',1);

/*Table structure for table `notifications` */

DROP TABLE IF EXISTS `notifications`;

CREATE TABLE `notifications` (
  `id` char(36) COLLATE utf8mb4_unicode_ci NOT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notifiable_id` bigint(20) unsigned NOT NULL,
  `data` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `notifications_notifiable_type_notifiable_id_index` (`notifiable_type`,`notifiable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `notifications` */

insert  into `notifications`(`id`,`type`,`notifiable_type`,`notifiable_id`,`data`,`read_at`,`created_at`,`updated_at`) values 
('5058be5f-1871-41b6-a186-e1ebc9981a35','App\\Notifications\\SituationUpdated','App\\User',1,'{\"user_id\":3,\"user_name\":\"user\",\"occurrence\":5,\"status\":\"Atendido\"}','2019-05-06 11:12:21','2019-05-06 11:12:04','2019-05-06 11:12:21'),
('604b07ef-ea01-4689-87b8-8b41029339be','App\\Notifications\\OccurrenceCreated','App\\User',2,'{\"user_id\":1,\"user_name\":\"Admin\",\"occurrence\":5}','2019-05-06 10:11:00','2019-05-06 09:51:56','2019-05-06 10:11:00'),
('b987bcc5-de6a-440c-92e3-bcb15661a4e9','App\\Notifications\\OccurrenceCreated','App\\User',2,'{\"user_id\":1,\"user_name\":\"Admin\",\"occurrence\":4}','2019-05-06 10:11:16','2019-05-06 09:29:39','2019-05-06 10:11:16'),
('c56e1a78-31d3-4d92-ac1f-e216a7d91816','App\\Notifications\\OccurrenceCreated','App\\User',3,'{\"user_id\":1,\"user_name\":\"Admin\",\"occurrence\":5}','2019-05-06 10:02:19','2019-05-06 09:51:56','2019-05-06 10:02:19');

/*Table structure for table `occurrence_user` */

DROP TABLE IF EXISTS `occurrence_user`;

CREATE TABLE `occurrence_user` (
  `occurrence_id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  PRIMARY KEY (`occurrence_id`,`user_id`),
  KEY `occurrence_user_ibfk_2` (`user_id`),
  CONSTRAINT `occurrence_user_ibfk_1` FOREIGN KEY (`occurrence_id`) REFERENCES `occurrences` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `occurrence_user_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `occurrence_user` */

insert  into `occurrence_user`(`occurrence_id`,`user_id`) values 
(1,2),
(1,3),
(4,1),
(4,2),
(5,2),
(5,3);

/*Table structure for table `occurrences` */

DROP TABLE IF EXISTS `occurrences`;

CREATE TABLE `occurrences` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `situation_id` int(10) unsigned NOT NULL,
  `from_user_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `message` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `details` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_closed` tinyint(1) NOT NULL,
  `closed_at` datetime DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `occurrences_from_user_id_foreign` (`from_user_id`),
  KEY `occurrences_situation_id_foreign` (`situation_id`),
  CONSTRAINT `occurrences_from_user_id_foreign` FOREIGN KEY (`from_user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `occurrences_situation_id_foreign` FOREIGN KEY (`situation_id`) REFERENCES `situations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `occurrences` */

insert  into `occurrences`(`id`,`situation_id`,`from_user_id`,`name`,`message`,`phone`,`email`,`details`,`is_closed`,`closed_at`,`created_at`,`updated_at`) values 
(1,1,1,'Thiago Silva','teste','(14)99176-6702','thi.csilva@hotmail.com',NULL,0,NULL,'2019-05-06 09:27:31','2019-05-06 13:39:30'),
(2,2,1,'Thiago Silva','teste','(14)99176-6702','thi.csilva@hotmail.com',NULL,0,NULL,'2019-05-06 09:27:57','2019-05-06 09:27:57'),
(3,2,1,'Thiago Silva','teste','(14)99176-6702','thi.csilva@hotmail.com',NULL,0,NULL,'2019-05-06 09:29:03','2019-05-06 09:29:03'),
(4,2,1,'Thiago Silva','teste','(14)99176-6702','thi.csilva@hotmail.com',NULL,1,'2019-05-06 12:01:01','2019-05-06 09:29:38','2019-05-06 12:01:01'),
(5,2,1,'Thiago Silva','teste','(14)99176-6702','thi.csilva@hotmail.com',NULL,0,NULL,'2019-05-06 09:51:56','2019-05-06 12:07:21');

/*Table structure for table `password_resets` */

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `password_resets` */

/*Table structure for table `roles` */

DROP TABLE IF EXISTS `roles`;

CREATE TABLE `roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `slug` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_slug_unique` (`slug`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `roles` */

insert  into `roles`(`id`,`description`,`slug`,`permissions`,`created_at`,`updated_at`) values 
(1,'Administrador','administrador','{\"occurrences\":true,\"occurrences_list\":true,\"occurrences_add\":true,\"occurrences_edit\":true,\"occurrences_destroy\":true,\"occurrences_print\":true,\"occurrences_close\":true,\"situations\":true,\"situations_list\":true,\"situations_add\":true,\"situations_edit\":true,\"situations_destroy\":true,\"reports\":true,\"users\":true,\"users_list\":true,\"users_add\":true,\"users_edit\":true,\"users_destroy\":true,\"roles\":true,\"roles_list\":true,\"roles_add\":true,\"roles_edit\":true,\"roles_destroy\":true}','2019-04-26 14:34:39','2019-05-01 14:58:25'),
(2,'Usuário','usuario','{\"occurrences\":true,\"occurrences_list\":true,\"occurrences_print\":true,\"roles_list\":true,\"roles_add\":true,\"roles_edit\":true,\"roles_destroy\":true}','2019-05-01 10:06:18','2019-05-01 14:57:44');

/*Table structure for table `situations` */

DROP TABLE IF EXISTS `situations`;

CREATE TABLE `situations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `description` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `color` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT 0,
  `close_occurrence` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `situations` */

insert  into `situations`(`id`,`description`,`color`,`active`,`close_occurrence`,`created_at`,`updated_at`) values 
(1,'Em Aberto','#C0A70E',1,0,'2019-05-06 08:22:15','2019-05-06 08:22:15'),
(2,'Atendido','#50EF00',1,1,'2019-05-06 08:22:27','2019-05-06 11:10:17'),
(3,'Não Pego','#F30000',1,0,'2019-05-06 08:22:41','2019-05-06 08:22:41');

/*Table structure for table `users` */

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `role_id` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `not_listed` smallint(1) DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_role_id_foreign` (`role_id`),
  CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

/*Data for the table `users` */

insert  into `users`(`id`,`role_id`,`name`,`email`,`email_verified_at`,`password`,`avatar`,`remember_token`,`not_listed`,`created_at`,`updated_at`) values 
(1,1,'Admin','admin@admin.com',NULL,'$2y$10$Bv1.V7Vuu67HdHXMSDB.Z.lIwiSEUyx7dEJL/03SBkKSqx38N1y1q','','lO0Survc2DwjjyH1s4cw3QhhFytQnTsF58Bo8j9uMylClnBps8LwYAyrnL3Q',1,'2019-04-26 14:35:23','2019-05-01 18:49:28'),
(2,2,'Teste','teste@teste.com',NULL,'$2y$10$Bv1.V7Vuu67HdHXMSDB.Z.lIwiSEUyx7dEJL/03SBkKSqx38N1y1q','','',0,'2019-04-29 14:17:46','2019-05-01 15:18:56'),
(3,2,'User','user@user.com',NULL,'$2y$10$tOfn2oy3tgKRmKJMVhbbNe5JYgzhOPG3PQAlG/xl8k1W4KO61SrmC','',NULL,0,'2019-05-03 11:35:45','2019-05-06 11:16:22');

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
