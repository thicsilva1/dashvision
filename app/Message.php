<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    protected $fillable = ['user_id', 'occurrence_id', 'message'];

    protected $dates = ['created_at', 'updated_at'];

    public function situation()
    {
        return $this->belongsTo(Situation::class);
    }

    public function occurrence()
    {
        return $this->belongsTo(Occurrence::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class)->withDefault([
            'id' => 0,
            'name' => 'Anônimo',
        ]);
    }
}
