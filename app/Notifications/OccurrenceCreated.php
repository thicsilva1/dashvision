<?php

namespace App\Notifications;

use App\Occurrence;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Notification;

class OccurrenceCreated extends Notification implements ShouldQueue
{
    use Queueable;

    protected $fromUser;
    protected $occurrence;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct(User $fromUser, Occurrence $occurrence)
    {
        $this->fromUser = $fromUser;
        $this->occurrence = $occurrence;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['database'];
    }

    public function toDatabase($notifiable)
    {
        return [
            'user_id' => $this->fromUser->id,
            'user_name' => $this->fromUser->name,
            'occurrence' => $this->occurrence->id,
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
