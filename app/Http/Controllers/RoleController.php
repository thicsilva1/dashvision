<?php

namespace App\Http\Controllers;

use App\Http\Requests\RoleRequest;
use App\Role;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $roles = Role::all();
        return view('roles.index', compact('roles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('roles.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(RoleRequest $request)
    {
        $data = $request->all();
        $data['slug'] = Str::slug($data['description'], '_');
        $i = 0;
        while (Role::where('slug', $data['slug'])->count() > 0) {
            $data['slug'] = Str::slug($data['description'] . ' ' . $i, '_');
            $i++;
        }

        foreach ($data['permissions'] as $key => $permission) {
            $data['permissions'][$key] = (bool) $permission;
        }

        $role = Role::create($data);
        return redirect()->route('roles.index')->with('success', 'Perfil criado com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function edit(Role $role)
    {
        return view('roles.edit', compact('role'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, Role $role)
    {
        $data = $request->all();
        foreach ($data['permissions'] as $key => $permission) {
            $data['permissions'][$key] = (bool) $permission;
        }
        $role->update($data);

        return redirect()->route('roles.index')->with('success', 'Perfil atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        try {

            $role = $role->delete();
        } catch (QueryException $e) {

            return redirect()->back()->with('error', 'Não foi possível excluir o perfil pois ele está associado a um ou mais usuários');
        }

        return redirect()->back()->with('success', 'Perfil excluído com sucesso');
    }
}
