<?php

namespace App\Http\Controllers;

use App\Occurrence;
use App\Situation;
use App\User;
use Illuminate\Http\Request;

class ReportController extends Controller
{
    public function index(Request $request)
    {
        $users = User::all();
        $situations = Situation::all();
        $start_date = !empty($request->start_date) ? \Carbon\Carbon::createFromFormat('d/m/Y', $request->start_date)->toDateString() : $request->start_date;
        $end_date = !empty($request->end_date) ? \Carbon\Carbon::createFromFormat('d/m/Y', $request->end_date)->toDateString() : $request->end_date;
        $fromUser = $request->from_user_id;
        $toUser = $request->to_user_id;
        $status = $request->situation_id;
        $occurrences = [];
        if (!empty($start_date) || !empty($end_date) || !empty($fromUser) || !empty($toUser) || !empty($status)) {

            $occurrences = Occurrence::when($start_date, function ($q, $start_date) {
                return $q->whereRaw("DATE(created_at)>='$start_date'");
            })->when($end_date, function ($q, $end_date) {
                return $q->whereRaw("DATE(created_at)<='$end_date'");
            })->when($fromUser, function ($q, $fromUser) {
                return $q->where('from_user_id', $fromUser);
            })->when($toUser, function ($q, $toUser) {
                return $q->with('users')->whereHas('users', function ($qu) use ($toUser) {
                    $qu->where('user_id', $toUser);
                });
            })->when($status, function ($q, $status) {
                return $q->where('situation_id', $status);
            })->get();

        }

        return view('reports.index', compact('situations', 'users', 'occurrences'));
    }

    public function printOccurrence(Occurrence $occurrence)
    {
        return view('prints.individual', compact('occurrence'));
    }

    public function printVariousOccurrence(Request $request)
    {
        $start_date = !empty($request->start_date) ? \Carbon\Carbon::createFromFormat('d/m/Y', $request->start_date)->toDateString() : $request->start_date;
        $end_date = !empty($request->end_date) ? \Carbon\Carbon::createFromFormat('d/m/Y', $request->end_date)->toDateString() : $request->end_date;
        $fromUser = $request->from_user_id;
        $toUser = $request->to_user_id;
        $status = $request->situation_id;
        $occurrences = [];

        if (!empty($start_date) || !empty($end_date) || !empty($fromUser) || !empty($toUser) || !empty($status)) {

            $occurrences = Occurrence::when($start_date, function ($q, $start_date) {
                return $q->whereRaw("DATE(created_at)>='$start_date'");
            })->when($end_date, function ($q, $end_date) {
                return $q->whereRaw("DATE(created_at)<='$end_date'");
            })->when($fromUser, function ($q, $fromUser) {
                return $q->where('from_user_id', $fromUser);
            })->when($toUser, function ($q, $toUser) {
                return $q->with('users')->whereHas('users', function ($qu) use ($toUser) {
                    $qu->where('user_id', $toUser);
                });
            })->when($status, function ($q, $status) {
                return $q->where('situation_id', $status);
            })->get();

        }

        return view('prints.various', compact('occurrences'));
    }
}
