<?php

namespace App\Http\Controllers;

use App\Http\Requests\MessageRequest;
use App\Message;
use App\Occurrence;

class MessageController extends Controller
{

    public function store(MessageRequest $request, Occurrence $occurrence)
    {
        $data = $request->all();
        $data['user_id'] = auth()->user()->id;
        $data['occurrence_id'] = $occurrence->id;
        $message = Message::create($data);
        return redirect()->back()->with('success', 'Mensagem criada com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Message  $message
     * @return \Illuminate\Http\Response
     */
    public function destroy(Occurrence $occurrence, Message $message)
    {
        $message->delete();
        return redirect()->back()->with('success', 'Mensagem excluída com sucesso');
    }
}
