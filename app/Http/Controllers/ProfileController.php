<?php

namespace App\Http\Controllers;

use App\Http\Requests\PasswordRequest;
use App\Http\Requests\ProfileRequest;
use Hash;

class ProfileController extends Controller
{
    public function edit()
    {
        $user = auth()->user();
        return view('profile.index', compact('user'));
    }

    public function update(ProfileRequest $request)
    {
        $user = auth()->user();
        $data = $request->all();
        $filename = $user->avatar;
        if ($request->has('avatar') && $request->file('avatar')->isValid()) {
            $file = $request->file('avatar');
            $extension = $file->extension();
            $path = public_path() . '/img/avatars/';
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;
            $file->move($path, $filename);

            if (!empty($user->avatar) && file_exists($path . $user->avatar)) {
                unlink($path . $user->avatar);
            }
        }
        $data['avatar'] = $filename;
        $user->update($data);
        return redirect()->back()->with('success', 'Perfil atualizado com sucesso');
    }

    public function updatePassword(PasswordRequest $request)
    {
        $user = auth()->user();
        $data = $request->all();
        if (!Hash::check($request->old_password, auth()->user()->getAuthPassword())) {
            return redirect()->back()->with('error', 'Senha antiga está incorreta');
        }
        $data['password'] = bcrypt($data['password']);
        $user->update($data);
        return redirect()->back()->with('success', 'Senha atualizada com sucesso');
    }
}
