<?php

namespace App\Http\Controllers;

use App\Http\Requests\OccurrenceRequest;
use App\Notifications\OccurrenceCreated;
use App\Notifications\SituationUpdated;
use App\Occurrence;
use App\Situation;
use App\User;
use Gate;
use Illuminate\Http\Request;

class OccurrenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $situations = Situation::where('active', true)->get();

        $occurrences = Occurrence::getModel();

        if ($request->query('situation')) {
            $occurrences = $occurrences->where('situation_id', $request->query('situation'));
        }
        if (Gate::allows('administrador')) {
            $occurrences = $occurrences->get();
        } else {
            $occurrences = $occurrences->whereHas('users', function ($q) {
                $q->where('user_id', auth()->user()->id);
            })
                ->where('is_closed', false)
                ->get();
        }
        return view('occurrences.index', compact('occurrences', 'situations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $situations = Situation::where('active', true)
            ->orderBy('description', 'asc')
            ->get();
        $users = User::all();
        return view('occurrences.create', compact('situations', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\OccurrenceRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(OccurrenceRequest $request)
    {
        $data = $request->all();
        $data['from_user_id'] = auth()->user()->id;
        $data['is_closed'] = $request->has('is_closed');
        $occurrence = Occurrence::create($data);
        $users = $request->to_user_id;

        $occurrence->users()->sync($users);

        foreach ($occurrence->users as $user) {
            if ($occurrence->fromUser->id != $user->id) {
                $user->notify(new OccurrenceCreated($occurrence->fromUser, $occurrence));
            }
        }

        return redirect()->route('occurrences.index')->with('success', 'Caso cadastrado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Occurrence  $occurrence
     * @return \Illuminate\Http\Response
     */
    public function show(Occurrence $occurrence)
    {

        $situations = Situation::where('active', true)->get();
        if (Gate::denies('administrador')) {
            $hasUser = false;
            foreach ($occurrence->users as $user) {
                if ($user->id == auth()->user()->id) {
                    $hasUser = true;
                }
            }
            if (!$hasUser) {
                return redirect()->back()->with('warning', 'Você não tem permissão para acessar esse caso');
            }
        }

        return view('occurrences.show', compact('occurrence', 'situations'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Occurrence  $occurrence
     * @return \Illuminate\Http\Response
     */
    public function edit(Occurrence $occurrence)
    {
        $situations = Situation::where('active', true)
            ->orderBy('description', 'asc')
            ->get();
        $users = User::all();

        return view('occurrences.edit', compact('occurrence', 'situations', 'users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\OccurrenceRequest  $request
     * @param  \App\Occurrence  $occurrence
     * @return \Illuminate\Http\Response
     */
    public function update(OccurrenceRequest $request, Occurrence $occurrence)
    {
        $data = $request->all();
        $occurrence->update($data);

        $users = $request->to_user_id;

        $occurrence->users()->sync($users);
        return redirect()->route('occurrences.index')->with('success', 'Caso atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Occurrence  $occurrence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Occurrence $occurrence)
    {
        $occurrence->delete();
        return redirect()->back()->with('success', 'Caso excluído com sucesso');
    }

    public function closeOccurrence(Request $request, Occurrence $occurrence)
    {
        $occurrence->is_closed = !$occurrence->is_closed;

        if ($occurrence->is_closed) {

            $occurrence->closed_at = \Carbon\Carbon::now();
        } else {
            $occurrence->closed_at = null;
        }
        $occurrence->save();

        if ($occurrence->is_closed) {
            return redirect()->back()->with('success', 'Caso encerrado com sucesso');
        }

        return redirect()->back()->with('success', 'Caso reaberto com sucesso');
    }

    public function updateSituation(Request $request, Occurrence $occurrence)
    {
        $data = $request->all();

        $occurrence->update($data);

        if ($occurrence->situation->close_occurrence) {
            $occurrence->is_closed = true;
            $occurrence->closed_at = \Carbon\Carbon::now();
            $occurrence->save();
        }

        $occurrence->fromUser->notify(new SituationUpdated(auth()->user(), $occurrence));

        if ($request->ajax) {
            return response()->json(['success' => true, 'message' => 'Status do caso foi atualizada com sucesso']);
        }

        return redirect()->back()->with('success', 'Status do caso foi atualizada com sucesso');
    }
}
