<?php

namespace App\Http\Controllers;

use App\Http\Requests\SituationRequest as Request;
use App\Situation;
use Illuminate\Database\QueryException;

class SituationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $situations = Situation::all();

        return view('situations.index', compact('situations'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('situations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\SituationRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->all();
        $data['active'] = $request->has('active');
        $data['close_occurrence'] = $request->has('close_occurrence');
        $situation = Situation::create($data);
        return redirect()->route('situations.index')->with('success', 'Status criado com sucesso');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Situation  $situation
     * @return \Illuminate\Http\Response
     */
    public function show(Situation $situation)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Situation  $situation
     * @return \Illuminate\Http\Response
     */
    public function edit(Situation $situation)
    {
        return view('situations.edit', compact('situation'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\SituationRequest  $request
     * @param  \App\Situation  $situation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Situation $situation)
    {
        $data = $request->all();
        $data['active'] = $request->has('active');
        $data['close_occurrence'] = $request->has('close_occurrence');
        $situation->update($data);
        return redirect()->route('situations.index')->with('success', 'Status atualizado com sucesso');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Situation  $situation
     * @return \Illuminate\Http\Response
     */
    public function destroy(Situation $situation)
    {
        try {
            $situation->delete();
        } catch (QueryException $e) {

            return redirect()->back()->with('error', 'Não foi possível excluir o status pois ele está associado a um ou mais registros');
        }

        return redirect()->back()->with('success', 'Status excluído com sucesso');
    }
}
