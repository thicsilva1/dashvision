<?php

namespace App\Http\Controllers;

use App\Attachment;
use App\Http\Requests\AttachmentRequest;
use App\Occurrence;
use Illuminate\Http\Request;

class AttachmentController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttachmentRequest $request, Occurrence $occurrence)
    {
        $data = $request->all();
        // $data['user_id'] = auth()->user()->id;
        $data['occurrence_id'] = $occurrence->id;
        $filename = '';

        if (!empty($request->file('filename'))) {
            $file = $request->file('filename');
            $extension = $file->extension();
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;
            $path = public_path() . '/uploads';
            $file->move($path, $filename);
        }
        $data['filename'] = $filename;

        if (empty($data['filename'])) {
            return redirect()->back()->with('error', 'Não foi possível salvar o anexo');
        }

        $attachment = Attachment::create($data);

        return redirect()->back()->with('success', 'Anexo incluído com sucesso');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function download(Occurrence $occurrence, Attachment $attachment)
    {
        $file = public_path() . '/uploads/' . $attachment->filename;
        if (!\file_exists($file)) {
            return response()->back()->with('error', 'Arquivo não existe');
        }
        return response()->download($file);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Occurrence $occurrence, Attachment $attachment)
    {
        if (!empty($attachment->filename)) {
            $file = public_path() . '/uploads/' . $attachment->filename;
            if (file_exists($file)) {
                unlink($file);
            }
        }
        $attachment->delete();
        return redirect()->back()->with('success', 'Anexo excluído com sucesso');
    }
}
