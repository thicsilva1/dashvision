<?php

namespace App\Http\Controllers;

use App\Occurrence;
use App\Situation;
use App\User;
use Gate;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $situations = Situation::where('active', true)->get();
        /* Total de casos com contagem por mês dentro do ano presente */
        $occurrences = Occurrence::count();
        $occurrencesPerMonth = Occurrence::selectRaw('MONTH(created_at) as month, count(*) as total')
            ->groupBy(DB::Raw('DATE_FORMAT(created_at,"%Y-%m")'))
            ->whereRaw('YEAR(created_at)=YEAR(CURRENT_DATE)')
            ->pluck('total', 'month');

        /* Total de casos encerrados com a contagem por mês dentro do ano presente */
        $occurrencesClosed = Occurrence::where('is_closed', true)->count();
        $occurrencesClosedPerMonth = Occurrence::selectRaw('MONTH(closed_at) as month, count(*) as total')
            ->groupBy(DB::Raw('DATE_FORMAT(closed_at,"%Y-%m")'))
            ->where('is_closed', true)
            ->whereRaw('YEAR(closed_at)=YEAR(CURRENT_DATE)')
            ->pluck('total', 'month');

        $situationsCount = Situation::where('active', true)->count();
        $situationsPerMonth = Situation::selectRaw('MONTH(created_at) as month, count(*) as total')
            ->groupBy(DB::Raw('DATE_FORMAT(created_at,"%Y-%m")'))
            ->whereRaw('YEAR(created_at)=YEAR(CURRENT_DATE)')
            ->where('active', true)
            ->pluck('total', 'month');

        $usersCount = User::count();
        $usersPerMonth = User::selectRaw('MONTH(created_at) as month, count(*) as total')
            ->groupBy(DB::Raw('DATE_FORMAT(created_at,"%Y-%m")'))
            ->whereRaw('YEAR(created_at)=YEAR(CURRENT_DATE)')
            ->pluck('total', 'month');

        $occurrencesPanel = Occurrence::selectRaw('count(*) as total, situation_id');
        if (Gate::denies('administrador')) {
            $occurrencesPanel = $occurrencesPanel->whereHas('users', function ($q) {
                $q->where('user_id', auth()->user()->id);
            })
                ->where('is_closed', false)
                ->groupBy('situation_id')
                ->get();
        } else {
            $occurrencesPanel = $occurrencesPanel->where('is_closed', false)->groupBy('situation_id')->get();
        }

        $occurrencesFromUser = Occurrence::selectRaw('DAY(created_at) as day, count(*) as total')
            ->groupBy('from_user_id')
            ->whereRaw('YEAR(created_at)=YEAR(CURRENT_DATE)')
            ->whereRaw('MONTH(created_at)=MONTH(CURRENT_DATE)')
            ->pluck('total', 'day');

        return view('dashboard.index', compact('situations', 'occurrences', 'occurrencesPerMonth', 'occurrencesClosed', 'occurrencesClosedPerMonth', 'situationsCount', 'situationsPerMonth', 'usersCount', 'usersPerMonth', 'occurrencesPanel', 'occurrencesFromUser'));
    }

    public function notifications()
    {
        return auth()->user()->unreadNotifications()->limit(5)->get()->toArray();
    }
}
