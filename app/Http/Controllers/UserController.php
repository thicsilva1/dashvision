<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserRequest;
use App\Role;
use App\User;
use Illuminate\Database\QueryException;

class UserController extends Controller
{

    public function index()
    {
        $users = User::all();
        return view('users.index', compact('users'));
    }

    public function create()
    {
        $roles = Role::all();
        return view('users.create', compact('roles'));
    }

    public function store(UserRequest $request)
    {
        $data = $request->all();
        $data['password'] = bcrypt($data['password']);
        $filename = '';
        if ($request->has('avatar') && $request->file('avatar')->isValid()) {
            $file = $request->file('avatar');
            $extension = $file->extension();
            $path = public_path() . '/img/avatars';
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;
            $file->move($path, $filename);
        }
        $data['avatar'] = $filename;
        $user = User::create($data);

        return redirect()->route('users.index')->with('success', 'Usuário criado com sucesso');
    }

    public function edit(User $user)
    {
        $roles = Role::all();
        return view('users.edit', compact('user', 'roles'));
    }

    public function update(UserRequest $request, User $user)
    {
        $data = $request->all();
        if ($request->has('password')) {
            if (empty($request->password)) {
                unset($data['password']);
            } else {
                $data['password'] = bcrypt($data['password']);
            }
        }
        $filename = $user->avatar;
        if ($request->has('avatar') && $request->file('avatar')->isValid()) {
            $file = $request->file('avatar');
            $extension = $file->extension();
            $path = public_path() . '/img/avatars/';
            $filename = md5($file->getClientOriginalName() . microtime()) . '.' . $extension;
            $file->move($path, $filename);

            if (!empty($user->avatar) && file_exists($path . $user->avatar)) {
                unlink($path . $user->avatar);
            }
        }
        $data['avatar'] = $filename;
        $user->update($data);

        return redirect()->route('users.index')->with('success', 'Usuário atualizado com sucesso');
    }

    public function destroy(User $user)
    {
        try {
            $user->delete();
        } catch (QueryException $e) {

            return redirect()->back()->with('error', 'Não foi possível excluir o usuário pois ele está associado a um ou mais registros');
        }

        return redirect()->back()->with('success', 'Usuário excluído com sucesso');
    }
}
