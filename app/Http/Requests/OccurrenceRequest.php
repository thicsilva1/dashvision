<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class OccurrenceRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'situation_id' => 'required',
            'to_user_id' => 'required',
            'name' => 'required',
            'message' => 'required',
            'phone' => 'required',
            'email' => 'required|email',
        ];
    }
}
