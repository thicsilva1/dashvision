<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        if ($this->method() == 'PUT') {
            return [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $this->segment(2),
                'avatar' => 'mimes:jpeg,bmp,png',
                'role_id' => 'required',
            ];
        }
        return [
            'name' => 'required',
            'email' => 'required|email|unique:users',
            'password' => 'required|min:6',
            'avatar' => 'mimes:jpeg,bmp,png',
            'role_id' => 'required',
        ];
    }
}
