<?php

namespace App\Providers;

use Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();
        $this->registerPermissionsPolicies();

        //
    }

    public function registerPermissionsPolicies()
    {
        Gate::define('occurrences', function ($user) {
            return $user->hasAccess(['occurrences']);
        });

        Gate::define('occurrences_list', function ($user) {
            return $user->hasAccess(['occurrences_list']);
        });

        Gate::define('occurrences_add', function ($user) {
            return $user->hasAccess(['occurrences_add']);
        });

        Gate::define('occurrences_edit', function ($user) {
            return $user->hasAccess(['occurrences_edit']);
        });

        Gate::define('occurrences_destroy', function ($user) {
            return $user->hasAccess(['occurrences_destroy']);
        });

        Gate::define('occurrences_print', function ($user) {
            return $user->hasAccess(['occurrences_print']);
        });

        Gate::define('occurrences_close', function ($user) {
            return $user->hasAccess(['occurrences_close']);
        });

        Gate::define('situations', function ($user) {
            return $user->hasAccess(['situations']);
        });

        Gate::define('situations_list', function ($user) {
            return $user->hasAccess(['situations_list']);
        });

        Gate::define('situations_add', function ($user) {
            return $user->hasAccess(['situations_add']);
        });

        Gate::define('situations_edit', function ($user) {
            return $user->hasAccess(['situations_edit']);
        });

        Gate::define('situations_destroy', function ($user) {
            return $user->hasAccess(['situations_destroy']);
        });

        Gate::define('reports', function ($user) {
            return $user->hasAccess(['reports']);
        });

        Gate::define('users', function ($user) {
            return $user->hasAccess(['users']);
        });

        Gate::define('users_list', function ($user) {
            return $user->hasAccess(['users_list']);
        });

        Gate::define('users_add', function ($user) {
            return $user->hasAccess(['users_add']);
        });

        Gate::define('users_edit', function ($user) {
            return $user->hasAccess(['users_edit']);
        });

        Gate::define('users_destroy', function ($user) {
            return $user->hasAccess(['users_destroy']);
        });

        Gate::define('roles', function ($user) {
            return $user->hasAccess(['roles']);
        });

        Gate::define('roles_list', function ($user) {
            return $user->hasAccess(['roles_list']);
        });

        Gate::define('roles_add', function ($user) {
            return $user->hasAccess(['roles_add']);
        });

        Gate::define('roles_edit', function ($user) {
            return $user->hasAccess(['roles_edit']);
        });

        Gate::define('roles_destroy', function ($user) {
            return $user->hasAccess(['roles_destroy']);
        });

        Gate::define('administrador', function ($user) {
            return $user->inRole('administrador');
        });

    }
}
