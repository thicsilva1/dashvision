<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class AuthUser extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'role_id', 'avatar',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'not_listed',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $table = 'users';

    public function role()
    {
        return $this->belongsTo(Role::class);
    }

    public function hasAccess(array $permissions): bool
    {
        if ($this->role->hasAccess($permissions)) {
            return true;
        }
        return false;
    }

    public function inRole(string $roleSlug)
    {
        return $this->role()->where('slug', $roleSlug)->count() == 1;
    }

    public function occurrences()
    {
        return $this->belongsToMany(Occurrence::class);
    }

}
