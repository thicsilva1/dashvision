<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    protected $fillable = ['description', 'slug', 'permissions'];

    protected $dates = ['created_at', 'updated_at'];

    protected $casts = ['permissions' => 'array'];

    public function users()
    {
        return $this->hasMany(User::class);
    }

    public function hasAccess(array $permissions): bool
    {
        foreach ($permissions as $permission) {
            if ($this->hasPermission($permission)) {
                return true;
            }
        }
        return false;
    }

    protected function hasPermission(string $permission)
    {
        return $this->permissions[$permission] ?? false;
    }
}
