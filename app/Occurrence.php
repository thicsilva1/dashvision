<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Occurrence extends Model
{
    protected $fillable = ['situation_id', 'from_user_id', 'name', 'message', 'phone', 'email', 'details', 'is_closed', 'closed_at'];

    protected $dates = ['created_at', 'updated_at', 'closed_at'];

    public function situation()
    {
        return $this->belongsTo(Situation::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

    public function fromUser()
    {
        return $this->belongsTo(User::class)->withDefault([
            'id' => 0,
            'name' => 'Anônimo',
        ]);
    }

    public function users()
    {
        return $this->belongsToMany(User::class);
    }
}
