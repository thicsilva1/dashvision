<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Situation extends Model
{
    protected $fillable = ['description', 'color', 'active', 'close_occurrence'];

    protected $dates = ['created_at', 'updated_at'];

    protected $cast = ['active' => 'boolean'];

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function occurrences()
    {
        return $this->hasMany(Occurrence::class);
    }
}
