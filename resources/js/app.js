window._ = require('lodash');
window.$ = window.jQuery = require('jquery');

var notifications = [];

const NOTIFICATION_TYPES = {
	occurrence: 'App\\Notifications\\OccurrenceCreated',
	situation: 'App\\Notifications\\SituationUpdated'
};

$(document).ready(function() {
	// check if there's a logged in user
	if (Laravel.userId) {
		$.get('/notifications', function(data) {
			addNotifications(data, '#notifications');
		});
	}
});

function addNotifications(newNotifications, target) {
	notifications = _.concat(notifications, newNotifications);
	// show only last 5 notifications
	notifications.slice(0, 5);
	showNotifications(notifications, target);
}

function showNotifications(notifications, target) {
	if (notifications.length) {
		var htmlElements = notifications.map(function(notification) {
			return makeNotification(notification);
		});
		$(target + 'Menu').html(htmlElements.join(''));
		$(target).html(
			'<i class="icon-bell"></i><span class="badge badge-pill badge-danger">' + notifications.length + '</span>'
		);
		let s = document.getElementById('sound-notification');
		s.play();
	} else {
		$(target + 'Menu').html('<div class="dropdown-header text-center"><strong>Sem notificações</strong></div>');
		$(target).html('<i class="icon-bell"></i>');
	}
}

function makeNotification(notification) {
	var to = routeNotification(notification);
	var notificationText = makeNotificationText(notification);
	return '<a class="dropdown-item" href="' + to + '"><i class="icon-layers"></i>' + notificationText + '</a>';
}

// get the notification route based on it's type
function routeNotification(notification) {
	var to = '?read=' + notification.id;
	var occurrence = notification.data.occurrence;
	if (notification.type === NOTIFICATION_TYPES.occurrence) {
		to = 'occurrences/' + occurrence + '/details' + to;
	}

	if (notification.type === NOTIFICATION_TYPES.situation) {
		to = 'occurrences/' + occurrence + '/details' + to;
	}
	return '/' + to;
}

// get the notification text based on it's type
function makeNotificationText(notification) {
	var text = '';
	if (notification.type === NOTIFICATION_TYPES.occurrence) {
		const name = notification.data.user_name;
		text += '<strong>' + name + '</strong> associou um caso a você';
	}
	if (notification.type === NOTIFICATION_TYPES.situation) {
		const id = notification.data.occurrence;
		const name = notification.data.user_name;
		const status = notification.data.status;
		text +=
			'<strong>' +
			name +
			'</strong> alterou o status do <strong> Caso #' +
			id +
			'</strong> para <strong>' +
			status +
			'</strong>';
	}
	return text;
}
