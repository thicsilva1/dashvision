@extends('layouts.app')

@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item active">Perfil</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item"><a href="#profile" data-toggle="tab" role="tab" aria-controls="profile" class="nav-link active">Perfil</a></li>
              <li class="nav-item"><a href="#password" data-toggle="tab" role="tab" aria-controls="password" class="nav-link">Senha</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="profile" role="tablist">
                <div class="card">
                  <form action="{{route('profile.update')}}" method="post" enctype="multipart/form-data">
                  @csrf
                  @method('PUT')
                    <div class="card-header">
                      <strong>Edição</strong>
                      <small>Perfil</small>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label for="name">Nome</label>
                        <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name', $user->name)}}">
                        @error('name')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                          </span>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email', $user->email)}}">
                        @error('email')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                          </span>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="avatar">Imagem</label>
                        <input type="file" name="avatar" id="avatar">
                        @error('avatar')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                          </span>
                        @enderror
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-success" type="submit">
                        <i class="icon cui-check"></i> Salvar
                      </button>
                      <a href="{{route('home')}}" class="btn btn-secondary"><i class="fa fa-close"></i> Cancelar</a>
                    </div>
                  </form>
                </div>
              </div>
              <div class="tab-pane" id="password" role="tablist">
                <div class="card">
                  <form action="{{route('profile.password')}}" method="post">
                  @csrf
                    <div class="card-header">
                      <strong>Edição</strong>
                      <small>Senha</small>
                    </div>
                    <div class="card-body">
                      <div class="form-group">
                        <label for="name">Senha Antiga</label>
                        <input type="password" name="old_password" id="old_password" class="form-control @error('old_password') is-invalid @enderror" value="{{old('old_password')}}">
                        @error('old_password')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                          </span>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="password">Nova Senha</label>
                        <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{old('password')}}">
                        @error('password')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                          </span>
                        @enderror
                      </div>
                      <div class="form-group">
                        <label for="password_confirmation">Confirme a Senha</label>
                        <input type="password" name="password_confirmation" id="password_confirmation" class="form-control @error('password') is-invalid @enderror" value="{{old('password_confirmation')}}">
                        @error('password_confirmation')
                          <span class="invalid-feedback" role="alert">
                            <strong>{{$message}}</strong>
                          </span>
                        @enderror
                      </div>
                    </div>
                    <div class="card-footer text-right">
                      <button class="btn btn-success" type="submit">
                        <i class="icon cui-check"></i> Salvar
                      </button>
                      <a href="{{route('home')}}" class="btn btn-secondary"><i class="fa fa-close"></i> Cancelar</a>
                    </div>
                  </form>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop
