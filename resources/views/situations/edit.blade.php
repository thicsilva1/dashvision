@extends('layouts.app')

@push('css')
  <link rel="stylesheet" href="/vendors/bootstrap-colorpicker/css/bootstrap-colorpicker.min.css">
@endpush

@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('situations.index')}}">Status</a></li>
    <li class="breadcrumb-item active">Editar</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div class="card">
              <form action="{{route('situations.update', $situation)}}" method="post">
              @csrf
              @method('PUT')
                <div class="card-header">
                  <strong>Status</strong>
                  <small>Edição</small>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="description">Descrição</label>
                    <input type="text" name="description" id="description" class="form-control @error('description') is-invalid @enderror" value="{{old('description', $situation->description)}}">
                    @error('description')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="color">Cor</label>
                    <input type="text" name="color" id="color" class="form-control @error('color') is-invalid @enderror" value="{{old('color', $situation->color)}}">
                    @error('color')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-2">
                        <label for="">Encerra Caso?</label>
                      </div>
                      <div class="col-md-10">
                        <label class="switch switch-label switch-outline-success-alt">
                          <input type="checkbox" name="close_occurrence" id="close_occurrence" class="switch-input" value="1" {{old('close_occurrence', $situation->close_occurrence)?'checked':''}}>
                          <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                        </label>
                      </div>
                    </div>
                    @error('close_occurrence')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <div class="row">
                      <div class="col-md-1">
                        <label for="">Ativo?</label>
                      </div>
                      <div class="col-md-11">
                        <label class="switch switch-label switch-outline-success-alt">
                          <input type="checkbox" name="active" id="active" class="switch-input" value="1" {{old('active', $situation->active)?'checked':''}}>
                          <span class="switch-slider" data-checked="✓" data-unchecked="✕"></span>
                        </label>
                      </div>
                    </div>
                    @error('active')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-success" type="submit">
                    <i class="icon cui-check"></i> Salvar
                  </button>
                  <a href="{{route('situations.index')}}" class="btn btn-secondary"><i class="fa fa-close"></i> Cancelar</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <script>
    $(function () {
      $('#color').colorpicker();
    });
  </script>
@endpush
