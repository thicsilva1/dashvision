@extends('layouts.app')
@push('css')
  <link rel="stylesheet" href="/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
  <style>
    @foreach($situations->groupBy('color') as $key=>$situation)
    .badge-{{substr($key,1,6)}}{
      background-color: {{$key}}40;
      color: {{$key}};
    }
    @endforeach;

  </style>
@endpush
@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item active">Casos</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                Casos
                <div class="card-header-actions">
                  @can('occurrences_add')
                  <a href="{{route('occurrences.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Cadastrar</a>
                  @endcan
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table datatable">
                    <thead>
                      <tr>
                        <td>Nome</td>
                        <td>Mensagem</td>
                        <td>Telefone</td>
                        <td>Status</td>
                        <td>Encerrado?</td>
                        <td>Associado para</td>
                        <td>Criado em</td>
                        <td>Ações</td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($occurrences as $occurrence)
                      <tr>
                        <td>{{$occurrence->name}}</td>
                        <td>{{substr_replace($occurrence->message, '...', 20)}}</td>
                        <td>{{$occurrence->phone}}</td>
                        <td><span class="badge badge-{{substr($occurrence->situation->color,1,6)}}">{{$occurrence->situation->description}}</span></td>
                        @if($occurrence->is_closed)
                        <td><span class="badge badge-success">Sim</span></td>
                        @else
                        <td><span class="badge badge-danger">Não</span></td>
                        @endif

                        <td>{{$occurrence->users->implode('name', ', ')}}</td>
                        <td>{{$occurrence->created_at->format('d/m/Y H:i')}}</td>
                        <td>
                          <div class="btn-group btn-group-sm">
                            <form action="{{route('occurrences.close', $occurrence)}}" method="post" id="close-{{$occurrence->id}}" style="display:none">@csrf</form>
                            <form action="{{ route('occurrences.destroy', $occurrence)}}" method="post">
                              @csrf
                              @method('DELETE')
                              <a href="{{route('occurrences.show', $occurrence)}}" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalhar"><i class="fa fa-newspaper-o"></i></a>
                              @can('occurrences_edit')
                              <a href="{{route('occurrences.edit', $occurrence)}}" class="btn btn-success" data-toggle="tooltip" data-placement="top" title data-original-title="Editar"><i class="fa fa-edit"></i></a>
                              @endcan
                              @can('occurrences_close')
                              @if(!$occurrence->is_closed)
                              <a href="#" class="btn btn-light" data-toggle="tooltip" data-placement="top" title data-original-title="Encerrar" onclick="event.preventDefault();document.getElementById('close-{{$occurrence->id}}').submit()"><i class="icon-lock"></i></a>
                              @else
                              <a href="#" class="btn btn-light" data-toggle="tooltip" data-placement="top" title data-original-title="Reabrir" onclick="event.preventDefault();document.getElementById('close-{{$occurrence->id}}').submit()"><i class="icon-lock-open"></i></a>
                              @endif
                              @endcan
                              @can('occurrences_destroy')
                              <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Excluir"><i class="fa fa-trash"></i></button>
                              @endcan
                            </form>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="/vendors/sweetalert/js/sweetalert.min.js"></script>
  <script>
    (function(){
      $('.datatable').dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
          "url": '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese-Brasil.json'
        },

        responsive: true,

        // save datatable state(pagination, sort, etc) in cookie.
        "bStateSave": true,

        "columnDefs": [ {
            "targets": [7],
            "orderable": false,
            "searchable": false
        }],
        "lengthMenu": [
            [5, 10, 20, 50, -1],
            [5, 10, 20, 50, "Todos"]
        ],

        "pageLength": 10,

        "order": [
            [1, "asc"]
        ],
      });
       $('.datatable').on('click', 'button.btn.btn-danger', function(e){
                e.preventDefault();
                let form = $(this).parents('form');
                swal({
                    title: "Confirma a exclusão?",
                    text: "Após excluído, não será possível recuperar as informações. Deseja continuar?",
                    type: "warning",
                    buttons: ['Cancelar', 'Confirmar'],
                    dangerMode: true,
                }).then(function(isConfirm){
                    if (isConfirm) {
                        form.submit();
                    }
                });
            });
    })(jQuery)
  </script>
@endpush
