@extends('layouts.app')

@push('css')
  <link rel="stylesheet" href="/vendors/select2/css/select2.min.css">
  <link rel="stylesheet" href="/vendors/select2/css/select2.bootstrap.css">

  <style>
    .ellipsis {
      text-overflow: ellipsis;
      white-space: nowrap;
      overflow: hidden;
    }
    .ellipsis:hover{
      overflow: visible;
      white-space: normal;
    }
    @foreach($situations->groupBy('color') as $key=>$situation)
    .badge-{{substr($key,1,6)}}{
      color: {{$key}};
      background-color: {{$key}}50;
    }
    .card-accent-{{substr($key,1,6)}}{
      border-top-color: {{$key}};
      border-top-width: 2px;
    }
    @endforeach;

  </style>
@endpush

@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('occurrences.index')}}">Casos</a></li>
    <li class="breadcrumb-item active">Detalhes</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-12">
            <div class="card card-accent-{{substr($occurrence->situation->color,1,6)}}">
              <div class="card-header">
                Caso <strong>#{{$occurrence->id}}</strong>
                <div class="card-header-actions">
                  <form action="{{route('occurrences.close', $occurrence)}}" method="post">
                    @csrf
                    @can('occurrences_close')
                    @if(!$occurrence->is_closed)
                    <button type="submit" class="btn btn-dark" data-toggle="tooltip" data-placement="top" title data-original-title="Encerrar"><i class="icon-lock"></i></button>
                    @else
                    <button type="submit" class="btn btn-dark" data-toggle="tooltip" data-placement="top" title data-original-title="Reabrir"><i class="icon-lock-open"></i></button>
                    @endif
                    @endcan
                    @can('occurrences_print')
                    <a href="#" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir" onclick="window.open('{{ route('occurrences.print', $occurrence) }}','popUpWindow','height=500,width=700,left=500,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');"><i class="fa fa-print"></i></a>
                    @endcan
                    </form>
                </div>
              </div>
              <div class="card-body">
                <div class="row">
                  <div class="col-sm-3"><small>Criado em <strong>{{$occurrence->created_at->format('d/m/Y H:i')}}</strong></small></div>
                  <div class="col-sm-3"><small>Por: <strong>{{$occurrence->fromUser->name}}</strong></small></div>
                  <div class="col-sm-3">
                    <small>Associado para:
                      <strong>{{$occurrence->users->implode('name', ', ')}}</strong>
                    </small></div>
                  <div class="col-sm-3"><small>Status:</small> <span class="badge badge-{{substr($occurrence->situation->color,1,6)}}">{{$occurrence->situation->description}}</span></div>
                </div>
                <div class="row mt-3">
                  <div class="col-sm-4">
                    <div><strong>{{$occurrence->name}}</strong></div>
                    <div>{{$occurrence->phone}}</div>
                    <div>{{$occurrence->email}}</div>
                  </div>
                  <div class="col-sm-8">
                    <div>{{$occurrence->message}}</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>

        <div class="row">
          <div class="col-md-12">
            <ul class="nav nav-tabs" role="tablist">
              <li class="nav-item"><a href="#situation" data-toggle="tab" role="tab" aria-controls="situation" class="nav-link active">Status</a></li>
              <li class="nav-item"><a href="#details" data-toggle="tab" role="tab" aria-controls="details" class="nav-link">Detalhes</a></li>
              <li class="nav-item"><a href="#attachments" data-toggle="tab" role="tab" aria-controls="attachments" class="nav-link">Anexos</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="situation" role="tabpanel">
                <div class="row justify-content-center">
                  <div class="col-md-10">
                    <form action="{{route('occurrences.update.situation', $occurrence)}}" method="post">
                      @csrf
                      <div class="form-group">
                        <select name="situation_id" id="situation_id" class="form-control select2">
                          @foreach($situations as $situation)
                          <option value="{{$situation->id}}" {{$situation->id==$occurrence->situation_id?'selected':''}}>{{$situation->description}}</option>
                          @endforeach
                        </select>
                      </div>
                      <div class="form-group text-right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-refresh"></i> Atualizar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="details" role="tabpanel">
              @foreach($occurrence->messages as $message)
                <div class="row justify-content-center">
                  <div class="col-md-10">
                    <div class="card">
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-1">
                            <div class="avatar">
                              <img src="/img/avatars/{{empty(auth()->user()->avatar)?'avatar.png':auth()->user()->avatar}}" alt="Avatar" class="img-avatar">
                            </div>
                          </div>
                          <div class="col-md-9">
                            <div class="row">
                              <div class="col-md-12">
                                <div class="text-value-sm">{{$message->user->name}}</div>
                              </div>
                              <div class="col-md-12">
                                <div class="ellipsis">{{$message->message}}</div>
                              </div>
                            </div>
                          </div>
                          <div class="col-md-2 text-right">
                            <form action="{{route('occurrences.message.destroy', [$occurrence, $message])}}" method="post">
                              @csrf
                              @method('DELETE')
                              <button type="submit" class="btn btn-danger" data-toggle="tooltip" data-placement="top" title data-original-title="Excluir"><i class="fa fa-trash"></i></button>
                            </form>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              @endforeach
                <div class="row justify-content-center">
                  <div class="col-md-10">
                    <form action="{{route('occurrences.message.store', $occurrence)}}" method="post">
                      @csrf
                      <div class="form-group">
                        <label for="message">Mensagem</label>
                        <textarea name="message" id="message" class="form-control"></textarea>
                      </div>
                      <div class="form-group text-right">
                        <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Salvar</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              <div class="tab-pane" id="attachments" role="tabpanel">
                <div class="card-group mb-4">
                @foreach($occurrence->attachments as $attachment)
                  <div class="card">
                    <div class="card-header bg-info">
                      <i class="fa fa-file-o"></i>
                      {{$attachment->filename}}
                    </div>
                    <form action="{{route('occurrences.attachment.destroy', [$occurrence, $attachment])}}" method="post">
                      @csrf
                      @method('DELETE')
                      <div class="card-body">
                        <div class="row">
                          <div class="col-md-6">
                            <a href="{{route('occurrences.attachment.download', [$occurrence, $attachment])}}" class="btn btn-success btn-sm"><i class="fa fa-download"></i> Download</a>
                          </div>
                          <div class="col-md-6">
                            <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-trash"></i> Excluir</button>
                          </div>
                        </div>
                      </div>
                    </form>
                  </div>
                @endforeach
                </div>
                <div class="row justify-content-center">
                  <div class="col-md-10">
                    <form action="{{route('occurrences.attachment.store',$occurrence)}}" method="post" enctype="multipart/form-data">
                      @csrf
                      <div class="form-group">
                        <label for="filename">Arquivo</label>
                        <input type="file" name="filename" id="filename">
                      </div>
                      <div class="form-group text-right">
                        <button class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/select2/js/select2.min.js"></script>
  <script src="/vendors/select2/js/i18n/pt-BR.js"></script>
  <script>
    (function(){
      $('.select2').select2({theme: 'bootstrap'});
      @if($occurrence->is_closed)
      $('form input[type="text"]').prop('disabled', true);
      $('form textarea').prop('disabled', true);
      $('form select').prop('disabled', true);
      $('form input[type="file"]').prop('disabled', true);
      $('form button.btn.btn-success').prop('disabled', true);
      $('form button.btn.btn-danger').prop('disabled', true);
      @endif
    })(jQuery)
  </script>

@endpush
