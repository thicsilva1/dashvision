@extends('layouts.app')

@push('css')
  <link rel="stylesheet" href="/vendors/select2/css/select2.min.css">
  <link rel="stylesheet" href="/vendors/select2/css/select2.bootstrap.css">

  <style>
    @foreach($situations->groupBy('color') as $key=>$situation)
    .badge-{{substr($key,1,6)}}{
      background-color: {{$key}}40;
      color: {{$key}};
    }
    @endforeach;

  </style>
@endpush

@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('occurrences.index')}}">Casos</a></li>
    <li class="breadcrumb-item active">Editar</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-8 offset-md-2">
            <div class="card">
              <form action="{{route('occurrences.update', $occurrence)}}" method="post">
              @csrf
              @method('PUT')
                <div class="card-header">
                  <strong>Casos</strong>
                  <small>Edição</small>
                </div>
                <div class="card-body">
                  <fieldset class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name', $occurrence->name)}}">
                    @error('name')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </fieldset>
                  <div class="row">
                    <fieldset class="form-group col-md-6">
                      <label for="email">Email</label>
                      <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email', $occurrence->email)}}">
                      @error('email')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                        </span>
                      @enderror
                    </fieldset>
                    <fieldset class="form-group col-md-6">
                      <label for="phone">Telefone</label>
                      <input type="phone" name="phone" id="phone" class="form-control @error('phone') is-invalid @enderror" value="{{old('phone', $occurrence->phone)}}">
                      @error('phone')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                        </span>
                      @enderror
                    </fieldset>
                  </div>
                  <fieldset class="form-group">
                    <label for="message">Mensagem</label>
                    <textarea name="message" id="message" class="form-control @error('message') is-invalid @enderror">{{old('message', $occurrence->message)}}</textarea>
                    @error('message')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </fieldset>
                  <div class="row">

                    <fieldset class="form-group col-md-6">
                      <label for="to_user_id">Para</label>
                      <select name="to_user_id[]" id="to_user_id" class="form-control select2 @error('to_user_id') is-invalid @enderror" multiple>

                      @foreach($occurrence->users as $user)
                        <option value="{{$user->id}}" selected>{{$user->name}}</option>
                      @endforeach

                      </select>
                      @error('to_user_id')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                        </span>
                      @enderror
                    </fieldset>
                    <fieldset class="form-group col-md-6">
                      <label for="situation_id">Status</label>
                      <select name="situation_id" id="situation_id" class="form-control select2 @error('situation_id') is-invalid @enderror">
                      @foreach($situations as $situation)
                        <option value="{{$situation->id}}" {{old('situation_id', $occurrence->situation_id)==$situation->id?'selected':''}}>{{$situation->description}}</option>
                      @endforeach
                      </select>
                      @error('situation_id')
                        <span class="invalid-feedback" role="alert">
                          <strong>{{$message}}</strong>
                        </span>
                      @enderror
                    </fieldset>
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-success" type="submit">
                    <i class="icon cui-check"></i> Salvar
                  </button>
                  <a href="{{route('occurrences.index')}}" class="btn btn-secondary"><i class="fa fa-close"></i> Cancelar</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/select2/js/select2.min.js"></script>
  <script src="/vendors/select2/js/i18n/pt-BR.js"></script>
  <script src="/vendors/inputmask/js/jquery.inputmask.bundle.js"></script>
  <script>
    (function(){
      $('#to_user_id').select2({
        theme: 'bootstrap',
        tokenSeparators: [','],
        data: [
          @foreach($users as $user)
          {
          id: {{$user->id}},
          text: "{{$user->name}}",
          },
          @endforeach
        ]
      });
       $('#situation_id').select2({
        theme: 'bootstrap',
      });
      $('#phone').inputmask({"mask": ['(99)9999-9999', '(99)99999-9999'], "keepstatic":true});
      @if($occurrence->is_closed)
      $('input').each(function(){
        $(this).prop('disabled', true);
      })
      $('select').each(function(){
        $(this).prop('disabled', true);
      })
      $('textarea').each(function(){
        $(this).prop('disabled', true);
      })
      $('button').each(function(){
        $(this).prop('disabled', true);
      })
      @endif
    })(jQuery)
  </script>
@endpush
