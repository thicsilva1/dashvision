@extends('layouts.app')

@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('users.index')}}">Usuários</a></li>
    <li class="breadcrumb-item active">Cadastrar</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="card">
              <form action="{{route('users.store')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card-header">
                  <strong>Usuários</strong>
                  <small>Cadastro</small>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="name">Nome</label>
                    <input type="text" name="name" id="name" class="form-control @error('name') is-invalid @enderror" value="{{old('name')}}">
                    @error('name')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" name="email" id="email" class="form-control @error('email') is-invalid @enderror" value="{{old('email')}}">
                    @error('email')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="password">Senha</label>
                    <input type="password" name="password" id="password" class="form-control @error('password') is-invalid @enderror" value="{{old('password')}}">
                    @error('password')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="role_id">Perfil</label>
                    <select name="role_id" id="role_id" class="form-control @error('role_id') is-invalid @enderror">
                    @foreach($roles as $role)
                      <option value="{{$role->id}}" {{old('role_id')==$role->id?'selected':''}}>{{$role->description}}</option>
                    @endforeach
                    </select>
                    @error('role_id')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <div class="form-group">
                    <label for="avatar">Foto</label>
                    <input type="file" name="avatar" id="avatar">
                  </div>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-success" type="submit">
                    <i class="icon cui-check"></i> Salvar
                  </button>
                  <a href="{{route('users.index')}}" class="btn btn-secondary"><i class="fa fa-close"></i> Cancelar</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
  <script>
    $(function () {
      $('#color').colorpicker();
    });
  </script>
@endpush
