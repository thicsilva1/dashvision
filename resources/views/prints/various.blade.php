@extends('prints.layout')

@section('content')
@foreach($occurrences as $occurrence)
<div class="row justify-content-center d-print-break">
  <div class="col-md-10">
    <div class="card mt-5">
      <div class="card-header">Caso <strong>#{{$occurrence->id}}</strong></div>
      <div class="card-body">
        <div class="row">
          <div class="col-sm-3"><small>Criado em <strong>{{$occurrence->created_at->format('d/m/Y H:i')}}</strong></small></div>
          <div class="col-sm-3"><small>Por: <strong>{{$occurrence->fromUser->name}}</strong></small></div>
          <div class="col-sm-3">
            <small>Associado para:
              <strong>{{$occurrence->users->implode('name', ', ')}}</strong>
            </small>
          </div>
          <div class="col-sm-3"><small>Status: <strong>{{$occurrence->situation->description}}</strong></small></div>
        </div>
        <div class="row mt-3">
          <div class="col-sm-4">
            <div><strong>{{$occurrence->name}}</strong></div>
            <div>{{$occurrence->phone}}</div>
            <div>{{$occurrence->email}}</div>
          </div>
          <div class="col-sm-8">
            <div>{{$occurrence->message}}</div>
          </div>
        </div>
        @if($occurrence->messages->count()>0)
        <hr>
        <h3>Detalhes</h3>
        @foreach($occurrence->messages as $message)
        <div class="row mt-2 text-right">
          <div class="col-sm-12">
            <small><strong>{{$message->user->name}} </strong> comentou em <strong>{{$message->created_at->format('d/m/Y \à\s H:i')}}</strong></small>
          </div>
        </div>
        <div class="row mt-1 text-justify">
          <div class="col-sm-12">
            {{$message->message}}
          </div>
        </div>
        @endforeach
        @endif
      </div>
    </div>
  </div>
</div>
@endforeach
@stop
