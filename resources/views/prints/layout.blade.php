<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Icons-->
    <link href="/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Main styles for this application-->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    <style>
      @media print {
        .d-print-break {
          page-break-inside: avoid;
        }
      }

    </style>
    <script>
      window.print();
    </script>

</head>
<body class="app">
  <div class="app-body">
    <main class="main">
      <div class="row">
        <div class="col-md-4">
          <img class="navbar-brand-full ml-5" src="/img/logo.png" height="60" alt="Fenix Investigações">
        </div>
        <div class="col-md-8">
          <h1>Sistema de Controle de Casos</h1>
        </div>
      </div>

      @yield('content')
    </main>
  </div>
  <footer class="app-footer">
    <div>
      <span>Gerado em {{date('d/m/Y \à\s H:i')}}</span>
    </div>
    <div class="ml-auto">
      <span>&copy; {{date('Y')}} <a href="https://fenixinvestigacoes.com.br">Fenix Investigações</a></span>
    </div>
  </footer>

  <!-- CoreUI and necessary plugins-->
  <script src="/vendors/jquery/js/jquery.min.js"></script>
  <script src="/vendors/popper.js/js/popper.min.js"></script>
  <script src="/vendors/bootstrap/js/bootstrap.min.js"></script>
  <script src="/vendors/pace-progress/js/pace.min.js"></script>
  <script src="/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
  <script src="/vendors/toastr/js/toastr.min.js"></script>
  <script src="/vendors/@coreui/coreui/js/coreui.min.js"></script>
  <script src="/js/tooltips.js"></script>
</body>
</html>
