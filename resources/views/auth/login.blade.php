@extends('auth.layout')

@section('content')
  <div class="row justify-content-center">
    <div class="col-md-6">
      <div class="card-group">
        <div class="card p-4">
          <div class="card-body">
            <form action="{{route('login')}}" method="post">
              @csrf
              <h1>Login</h1>
              <p class="text-muted">Entre com os dados de sua conta</p>
              <div class="input-group mb-3">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-user"></i>
                  </span>
                </div>
                <input class="form-control @error('email') is-invalid @enderror" name="email" id="email" type="text" placeholder="Email">
                @error('email')
                <span class="invalid-feedback" role="alert">
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div>
              <div class="input-group mb-4">
                <div class="input-group-prepend">
                  <span class="input-group-text">
                    <i class="icon-lock"></i>
                  </span>
                </div>
                <input class="form-control @error('password') is-invalid @enderror" type="password" name="password" id="password" placeholder="Senha">
                @error('password')
                <span class="invalid-feedback" role="alert">
                  <strong>{{$message}}</strong>
                </span>
                @enderror
              </div>
              <div class="input-group mb-4">
                <div class="form-checkbox checkbox">
                  <input type="checkbox" name="remember" id="remember" class="form-check-input ml-0">
                  <label for="remember" class="form-check-label ml-3">Manter conectado</label>
                </div>
              </div>
              <div class="row">
                <div class="col-6">
                  <button class="btn btn-primary px-4" type="submit">Login</button>
                </div>
                <div class="col-6 text-right">
                  <button class="btn btn-link px-0" type="button">Esqueceu a senha?</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
