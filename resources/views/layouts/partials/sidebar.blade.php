<div class="sidebar">
  <nav class="sidebar-nav">
    <ul class="nav">
      <li class="nav-item">
        <a class="nav-link" href="{{route('home')}}">
          <i class="nav-icon cui-dashboard"></i> Dashboard
        </a>
      </li>
      @can('occurrences')
      <li class="nav-item nav-dropdown {{request()->is('occurrences*')?'open':''}}">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon icon-layers"></i> Casos</a>
        <ul class="nav-dropdown-items">
          @can('occurrences_add')
          <li class="nav-item">
            <a href="{{route('occurrences.create')}}" class="nav-link">
              <i class="nav-icon fa fa-plus"></i> Cadastrar</a>
          </li>
          @endcan
          @can('occurrences_list')
          <li class="nav-item">
            <a href="{{route('occurrences.index')}}" class="nav-link">
              <i class="nav-icon icon-magnifier"></i> Pesquisar</a>
          </li>
          @endcan
        </ul>
      </li>
      @endcan
      @can('situations')
      <li class="nav-item nav-dropdown {{request()->is('situations*')?'open':''}}">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon icon-directions"></i> Status</a>
        <ul class="nav-dropdown-items">
          @can('situations_add')
          <li class="nav-item">
            <a href="{{route('situations.create')}}" class="nav-link">
              <i class="nav-icon fa fa-plus"></i> Cadastrar</a>
          </li>
          @endcan
          @can('situations_list')
          <li class="nav-item">
            <a href="{{route('situations.index')}}" class="nav-link">
              <i class="nav-icon icon-magnifier"></i> Pesquisar</a>
          </li>
          @endcan
        </ul>
      </li>
      @endcan
      @can('reports')
      <li class="nav-item nav-dropdown {{request()->is('reports*')?'open':''}}">
        <a class="nav-link" href="{{route('reports.index')}}">
          <i class="nav-icon icon-printer"></i> Relatório</a>
      </li>
      @endcan
      @can('users')
      <li class="nav-title">Configurações</li>
      @endcan
      @can('users')
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon icon-people"></i> Usuários</a>
        <ul class="nav-dropdown-items">
          @can('users_add')
          <li class="nav-item">
            <a href="{{route('users.create')}}" class="nav-link">
              <i class="nav-icon fa fa-plus"></i> Cadastrar</a>
          </li>
          @endcan
          @can('users_list')
          <li class="nav-item">
            <a href="{{route('users.index')}}" class="nav-link">
              <i class="nav-icon icon-magnifier"></i> Pesquisar</a>
          </li>
          @endcan
        </ul>
      </li>
      @endcan
      @can('roles')
      <li class="nav-item nav-dropdown">
        <a class="nav-link nav-dropdown-toggle" href="#">
          <i class="nav-icon cui-lock-locked"></i> Perfis</a>
        <ul class="nav-dropdown-items">
          @can('roles_add')
          <li class="nav-item">
            <a href="{{route('roles.create')}}" class="nav-link">
              <i class="nav-icon fa fa-plus"></i> Cadastrar</a>
          </li>
          @endcan
          @can('roles_list')
          <li class="nav-item">
            <a href="{{route('roles.index')}}" class="nav-link">
              <i class="nav-icon icon-magnifier"></i> Pesquisar</a>
          </li>
          @endcan
        </ul>
      </li>
      @endcan
    </ul>
  </nav>
  <button class="sidebar-minimizer brand-minimizer" type="button"></button>
</div>
