<header class="app-header navbar">
  <button class="navbar-toggler sidebar-toggler d-lg-none mr-auto" type="button" data-toggle="sidebar-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a class="navbar-brand" href="{{route('home')}}">
    <img class="navbar-brand-full" src="/img/logo.png" height="40" alt="Fenix Investigações">
    <img class="navbar-brand-minimized" src="/img/logo_mini.png" height="40" alt="Fenix Investigações">
  </a>
  <button class="navbar-toggler sidebar-toggler d-md-down-none" type="button" data-toggle="sidebar-lg-show">
    <span class="navbar-toggler-icon"></span>
  </button>
  <ul class="nav navbar-nav ml-auto">
    <audio id="sound-notification">
      <source src="/sounds/appointed.mp3" type="audio/mpeg">
      <source src="/sounds/appointed.ogg" type="audio/ogg">
    </audio>

    <li class="nav-item dropdown d-md-down-none">
      <a class="nav-link" id="notifications" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <i class="icon-bell"></i>
      </a>
      <div class="dropdown-menu dropdown-menu-right dropdown-menu-xl" id="notificationsMenu">
        <div class="dropdown-header text-center">
          <strong>Sem notificações</strong>
        </div>
      </div>
    </li>
    <li class="nav-item dropdown">
      <a class="nav-link nav-link" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
        <img class="img-avatar" src="/img/avatars/{{empty(auth()->user()->avatar)?'avatar.png':auth()->user()->avatar}}" alt="admin@bootstrapmaster.com">
      </a>
      <div class="dropdown-menu dropdown-menu-right">
        <div class="dropdown-header text-center">
          <strong>Configurações</strong>
        </div>
        <a class="dropdown-item" href="{{route('profile.edit')}}">
          <i class="fa fa-user"></i> Perfil</a>
        <form action="{{route('logout')}}" method="post" id="logout" style="display:none">
          @csrf
        </form>
        <a class="dropdown-item" href="#" onclick="event.preventDefault();document.getElementById('logout').submit()">
          <i class="fa fa-lock"></i> Sair</a>
      </div>
    </li>
  </ul>
</header>
