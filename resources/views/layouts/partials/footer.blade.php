<footer class="app-footer">
  <div class="ml-auto">

    <span>&copy; {{date('Y')}} <a href="https://fenixinvestigacoes.com.br">Fenix Investigações</a></span>
  </div>
</footer>
