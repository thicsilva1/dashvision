@if (session('success'))

  <script>
      toastr.options.closeButton = true;
      toastr.success("{{session('success')}}", 'Sucesso')
  </script>
@endif

@if (session('error'))

  <script>
      toastr.options.closeButton = true;
      toastr.error("{{session('error')}}", 'Erro')
  </script>
@endif

@if (session('info'))

  <script>
      toastr.options.closeButton = true;
      toastr.info("{{session('info')}}", 'Informação')
  </script>
@endif

@if (session('warning'))

  <script>
      toastr.options.closeButton = true;
      toastr.warning("{{session('warning')}}", 'Atenção')
  </script>
@endif

@if ($errors->any())
  <script>
      toastr.options.closeButton = true;
      @foreach($errors->all() as $error)
          toastr.error("{{$error}}",'Atenção')
      @endforeach

  </script>
@endif
