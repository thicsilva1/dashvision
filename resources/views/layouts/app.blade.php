<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <link rel="icon" href="/img/cropped-favicon-32x32.png" type="image/x-icon" sizes="32x32">
    <link rel="icon" href="/img/cropped-favicon-192x192.png" type="image/x-icon" sizes="192x192">
    <link rel="apple-touch-icon-precomposed" href="/img/cropped-favicon-180x180.png">

    <!-- Icons-->
    <link href="/vendors/@coreui/icons/css/coreui-icons.min.css" rel="stylesheet">
    <link href="/vendors/flag-icon-css/css/flag-icon.min.css" rel="stylesheet">
    <link href="/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <link href="/vendors/simple-line-icons/css/simple-line-icons.css" rel="stylesheet">

    <!-- Main styles for this application-->
    <link href="/css/style.css" rel="stylesheet">
    <link href="/vendors/pace-progress/css/pace.min.css" rel="stylesheet">
    <link href="/vendors/toastr/css/toastr.min.css" rel="stylesheet">

    <!-- Custom styles -->
    @stack('css')

    <!-- Global site tag (gtag.js) - Google Analytics-->
    <script async="" src="https://www.googletagmanager.com/gtag/js?id=UA-118965717-3"></script>
    <script>
      window.dataLayer = window.dataLayer || [];

      function gtag() {
        dataLayer.push(arguments);
      }
      gtag('js', new Date());
      // Shared ID
      gtag('config', 'UA-118965717-3');
      // Bootstrap ID
      gtag('config', 'UA-118965717-5');
    </script>
    <script>
      window.Laravel = <?php echo json_encode(['csrfToken' => csrf_token()]); ?>

    </script>

    <!-- This makes the current user's id available in javascript -->
    @if(!auth()->guest())
      <script>
          window.Laravel.userId = <?php echo auth()->user()->id; ?>
      </script>
    @endif
</head>
<body class="app header-fixed sidebar-fixed aside-menu-fixed sidebar-lg-show">
  <!-- header -->
  @include('layouts.partials.header')
  <div class="app-body">
    @include('layouts.partials.sidebar')

    @yield('content')
    @include('layouts.partials.aside')
  </div>
  @include('layouts.partials.footer')
  <!-- CoreUI and necessary plugins-->
  <script src="/js/app.js"></script>
  <script src="/vendors/jquery/js/jquery.min.js"></script>
  <script src="/vendors/popper.js/js/popper.min.js"></script>
  <script src="/vendors/bootstrap/js/bootstrap.min.js"></script>
  <script src="/vendors/pace-progress/js/pace.min.js"></script>
  <script src="/vendors/perfect-scrollbar/js/perfect-scrollbar.min.js"></script>
  <script src="/vendors/toastr/js/toastr.min.js"></script>
  <script src="/vendors/@coreui/coreui/js/coreui.min.js"></script>
  <script src="/js/tooltips.js"></script>
  @include('layouts.partials.toastr')
  <!-- Plugins and scripts required by this view-->
  @stack('scripts')
</body>
</html>
