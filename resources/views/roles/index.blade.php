@extends('layouts.app')
@push('css')
  <link rel="stylesheet" href="/vendors/datatables.net-bs4/css/dataTables.bootstrap4.min.css">
@endpush
@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item active">Perfis</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                Perfis
                <div class="card-header-actions">
                  @can('roles_add')
                  <a href="{{route('roles.create')}}" class="btn btn-primary"><i class="fa fa-plus"></i> Cadastrar</a>
                  @endcan
                </div>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table datatable">
                    <thead>
                      <tr>
                        <td>Descrição</td>
                        <td>Criado em</td>
                        <td>Ações</td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($roles as $role)
                      <tr>
                        <td>{{$role->description}}</td>
                        <td>{{$role->created_at->format('d/m/Y H:i')}}</td>
                        <td>
                          <div class="btn-group btn-group-sm">
                            <form action="{{ route('roles.destroy', $role)}}" method="post">
                              @csrf
                              @method('DELETE')
                              @can('roles_edit')
                              <a href="{{route('roles.edit', $role)}}" class="btn btn-success"><i class="fa fa-edit"></i></a>
                              @endcan
                              @can('roles_destroy')
                              <button type="submit" class="btn btn-danger"><i class="fa fa-trash"></i></button>
                              @endcan
                            </form>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/datatables.net/js/jquery.dataTables.min.js"></script>
  <script src="/vendors/datatables.net-bs4/js/dataTables.bootstrap4.min.js"></script>
  <script src="/vendors/sweetalert/js/sweetalert.min.js"></script>
  <script>
    (function(){
      $('.datatable').dataTable({
        // Internationalisation. For more info refer to http://datatables.net/manual/i18n
        "language": {
          "url": '//cdn.datatables.net/plug-ins/3cfcc339e89/i18n/Portuguese-Brasil.json'
        },

        responsive: true,

        // save datatable state(pagination, sort, etc) in cookie.
        "bStateSave": true,

        "columnDefs": [ {
            "targets": [2],
            "orderable": false,
            "searchable": false
        }],
        "lengthMenu": [
            [5, 10, 20, 50, -1],
            [5, 10, 20, 50, "Todos"]
        ],

        "pageLength": 10,

        "order": [
            [1, "asc"]
        ],
      });
       $('.datatable').on('click', 'button.btn.btn-danger', function(e){
                e.preventDefault();
                let form = $(this).parents('form');
                swal({
                    title: "Confirma a exclusão?",
                    text: "Após excluído, não será possível recuperar as informações. Deseja continuar?",
                    type: "warning",
                    buttons: ['Cancelar', 'Confirmar'],
                    dangerMode: true,
                }).then(function(isConfirm){
                    if (isConfirm) {
                        form.submit();
                    }
                });
            });
    })(jQuery)
  </script>
@endpush
