@extends('layouts.app')

@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item"><a href="{{route('roles.index')}}">Perfis</a></li>
    <li class="breadcrumb-item active">Cadastrar</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row justify-content-center">
          <div class="col-md-8">
            <div class="card">
              <form action="{{route('roles.store')}}" method="post" enctype="multipart/form-data">
              @csrf
                <div class="card-header">
                  <strong>Perfis</strong>
                  <small>Cadastro</small>
                </div>
                <div class="card-body">
                  <div class="form-group">
                    <label for="description">Descrição</label>
                    <input type="text" name="description" id="description" class="form-control @error('description') is-invalid @enderror" value="{{old('description')}}">
                    @error('description')
                      <span class="invalid-feedback" role="alert">
                        <strong>{{$message}}</strong>
                      </span>
                    @enderror
                  </div>
                  <fieldset class="form-group">
                    <legend>Casos</legend>
                    <div class="form-check checkbox">
                      <input class="form-check-input" type="checkbox" name="permissions[occurrences]" id="occurrences" value="1">
                      <label class="form-check-label" for="occurrences">Menu Casos</label>
                    </div>
                    <div class="form-group hide" id="occurrences-group">
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[occurrences_list]" id="occurrences_list" value="1">
                        <label class="form-check-label" for="occurrences_list">Pesquisar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[occurrences_add]" id="occurrences_add" value="1">
                        <label class="form-check-label" for="occurrences_add">Incluir</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[occurrences_edit]" id="occurrences_edit" value="1">
                        <label class="form-check-label" for="occurrences_edit">Editar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[occurrences_destroy]" id="occurrences_destroy" value="1">
                        <label class="form-check-label" for="occurrences_destroy">Excluir</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[occurrences_print]" id="occurrences_print" value="1">
                        <label class="form-check-label" for="occurrences_print">Imprimir</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[occurrences_close]" id="occurrences_close" value="1">
                        <label class="form-check-label" for="occurrences_close">Encerrar</label>
                      </div>
                    </div>
                  </fieldset>
                  <fieldset class="form-group">
                    <legend>Status</legend>
                    <div class="form-check checkbox">
                      <input type="checkbox" name="permissions[situations]" id="situations" class="form-check-input" value="1">
                      <label for="situations" class="form-check-label">Menu Status</label>
                    </div>
                    <div class="form-group hide" id="situations-group">
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[situations_list]" id="situations_list" value="1">
                        <label class="form-check-label" for="situations_list">Pesquisar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[situations_add]" id="situations_add" value="1">
                        <label class="form-check-label" for="situations_add">Incluir</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[situations_edit]" id="situations_edit" value="1">
                        <label class="form-check-label" for="situations_edit">Editar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[situations_destroy]" id="situations_destroy" value="1">
                        <label class="form-check-label" for="situations_destroy">Excluir</label>
                      </div>
                    </div>
                  </fieldset>
                  <fieldset class="form-group">
                    <legend>Relatório</legend>
                    <div class="form-check checkbox">
                      <input type="checkbox" name="permissions[reports]" id="reports" class="form-check-input" value="1">
                      <label for="reports" class="form-check-label">Menu Relatório</label>
                    </div>
                  </fieldset>
                  <fieldset class="form-group">
                    <legend>Usuários</legend>
                    <div class="form-check checkbox">
                      <input type="checkbox" name="permissions[users]" id="users" class="form-check-input" value="1">
                      <label for="users" class="form-check-label">Menu Usuários</label>
                    </div>
                    <div class="form-group hide" id="users-group">
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[users_list]" id="users_list" value="1">
                        <label class="form-check-label" for="users_list">Pesquisar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[users_add]" id="users_add" value="1">
                        <label class="form-check-label" for="users_add">Incluir</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[users_edit]" id="users_edit" value="1">
                        <label class="form-check-label" for="users_edit">Editar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[users_destroy]" id="users_destroy" value="1">
                        <label class="form-check-label" for="users_destroy">Excluir</label>
                      </div>
                    </div>
                  </fieldset>
                  <fieldset class="form-group">
                    <legend>Perfis</legend>
                    <div class="form-check checkbox">
                      <input type="checkbox" name="permissions[roles]" id="roles" class="form-check-input" value="1">
                      <label for="roles" class="form-check-label">Menu Perfis</label>
                    </div>
                    <div class="form-group hide" id="roles-group">
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[roles_list]" id="roles_list" value="1">
                        <label class="form-check-label" for="roles_list">Pesquisar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[roles_add]" id="roles_add" value="1">
                        <label class="form-check-label" for="roles_add">Incluir</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[roles_edit]" id="roles_edit" value="1">
                        <label class="form-check-label" for="roles_edit">Editar</label>
                      </div>
                      <div class="form-check form-check-inline mr-1">
                        <input class="form-check-input" type="checkbox" name="permissions[roles_destroy]" id="roles_destroy" value="1">
                        <label class="form-check-label" for="roles_destroy">Excluir</label>
                      </div>
                    </div>
                  </fieldset>
                </div>
                <div class="card-footer text-right">
                  <button class="btn btn-success" type="submit">
                    <i class="icon cui-check"></i> Salvar
                  </button>
                  <a href="{{route('roles.index')}}" class="btn btn-secondary"><i class="fa fa-close"></i> Cancelar</a>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script>
    $(function () {
      function toggleSituations(){
        if ($('#situations').prop('checked')){
          $('#situations-group').removeClass('hide')
        } else {
          $('#situations-group').addClass('hide')
        }
      }
      function toggleOccurrences(){
        if ($('#occurrences').prop('checked')){
          $('#occurrences-group').removeClass('hide')
        } else {
          $('#occurrences-group').addClass('hide')
        }
      }
      function toggleUsers(){
        if ($('#users').prop('checked')){
          $('#users-group').removeClass('hide')
        } else {
          $('#users-group').addClass('hide')
        }
      }
      function toggleRoles(){
        if ($('#roles').prop('checked')){
          $('#roles-group').removeClass('hide')
        } else {
          $('#roles-group').addClass('hide')
        }
      }

      toggleSituations();
      toggleOccurrences();
      toggleUsers();
      toggleRoles();

      $('#situations').on('click', function(){
        toggleSituations();
        if ($(this).prop('checked')){
          $('#situations-group input[type="checkbox"]').each(function(){
            $(this).prop('checked', true);
          })
        }
      })

      $('#occurrences').on('click', function(){
        toggleOccurrences();
        if ($(this).prop('checked')){
          $('#occurrences-group input[type="checkbox"]').each(function(){
            $(this).prop('checked', true);
          })
        }
      })

      $('#users').on('click', function(){
        toggleUsers();
        if ($(this).prop('checked')){
          $('#users-group input[type="checkbox"]').each(function(){
            $(this).prop('checked', true);
          })
        }
      })

      $('#roles').on('click', function(){
        toggleRoles();
        if ($(this).prop('checked')){
          $('#roles-group input[type="checkbox"]').each(function(){
            $(this).prop('checked', true);
          })
        }
      })

    });
  </script>
@endpush
