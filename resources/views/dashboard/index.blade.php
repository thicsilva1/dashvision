@extends('layouts.app')

@push('css')
  <style>
    @foreach($situations->groupBy('color') as $key=>$situation)
    .bg-{{substr($key,1,6)}}{
      background-color: {{$key}};
    }
    .text-{{substr($key,1,6)}}{
      color: {{$key}};
    }
    @endforeach;
  </style>
@endpush

@section('content')
<main class="main">
  <!-- Breadcrumb-->
  <ol class="breadcrumb">
    <li class="breadcrumb-item active">Home</li>
  </ol>
  <div class="container-fluid">
    <div class="animated fadeIn">
      @can('administrador')
      @include('dashboard.admin')
      @endcan
      @cannot('administrador')
      @include('dashboard.user')
      @endcannot
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/chart.js/js/Chart.min.js"></script>
  <script src="/vendors/@coreui/coreui-plugin-chartjs-custom-tooltips/js/custom-tooltips.min.js"></script>
  <script src="/js/main.js"></script>
  <script>
    (function(){
      $(document).ready(function(){
        Chart.defaults.global.pointHitDetectionRadius = 1;
        Chart.defaults.global.tooltips.enabled = false;
        Chart.defaults.global.tooltips.mode = 'index';
        Chart.defaults.global.tooltips.position = 'nearest';
        Chart.defaults.global.tooltips.custom = CustomTooltips; // eslint-disable-next-line no-unused-vars

        let occurrencesData = [<?php for ($i = 1; $i < 13; $i++) {
    if (isset($occurrencesPerMonth[$i])) {
        echo $occurrencesPerMonth[$i] . ', ';
    } else {
        echo '0, ';
    }
}?>]
        var cardChart1 = new Chart($('#occurrences-chart'), {
          type: 'line',
          data: {
            labels: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
            datasets: [
              {
                label: 'Casos cadastrados',
                backgroundColor: getStyle('--primary'),
                borderColor: 'rgba(255,255,255,.55)',
                data: occurrencesData
              }
            ]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            scales: {
              xAxes: [
                {
                  gridLines: {
                    color: 'transparent',
                    zeroLineColor: 'transparent'
                  },
                  ticks: {
                    fontSize: 2,
                    fontColor: 'transparent'
                  }
                }
              ],
              yAxes: [
                {
                  display: false,
                  ticks: {
                    display: false,
                    min: 0,
                    max: 100
                  }
                }
              ]
            },
            elements: {
              line: {
                borderWidth: 1
              },
              point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4
              }
            }
          }
        }); // eslint-disable-next-line no-unused-vars

        let closedData = [<?php for ($i = 1; $i < 13; $i++) {
    if (isset($occurrencesClosedPerMonth[$i])) {
        echo $occurrencesClosedPerMonth[$i] . ', ';
    } else {
        echo '0, ';
    }
}?>]
        var cardChart2 = new Chart($('#closed-chart'), {
          type: 'line',
          data: {
            labels: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
            datasets: [
              {
                label: 'Casos encerrados',
                backgroundColor: getStyle('--success'),
                borderColor: 'rgba(255,255,255,.55)',
                data: closedData
              }
            ]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            scales: {
              xAxes: [
                {
                  gridLines: {
                    color: 'transparent',
                    zeroLineColor: 'transparent'
                  },
                  ticks: {
                    fontSize: 2,
                    fontColor: 'transparent'
                  }
                }
              ],
              yAxes: [
                {
                  display: false,
                  ticks: {
                    display: false,
                    min: 0,
                    max: 100
                  }
                }
              ]
            },
            elements: {
              line: {
                tension: 0.00001,
                borderWidth: 1
              },
              point: {
                radius: 4,
                hitRadius: 10,
                hoverRadius: 4
              }
            }
          }
        }); // eslint-disable-next-line no-unused-vars

        let situationData = [<?php for ($i = 1; $i < 13; $i++) {
    if (isset($situationsPerMonth[$i])) {
        echo $situationsPerMonth[$i] . ', ';
    } else {
        echo '0, ';
    }
}?>]
        var cardChart3 = new Chart($('#situations-chart'), {
          type: 'line',
          data: {
            labels: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
            datasets: [
              {
                label: 'Status Cadastrados',
                backgroundColor: getStyle('--info'),
                borderColor: 'rgba(255,255,255,.55)',
                data: situationData
              }
            ]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            scales: {
              xAxes: [
                {
                  display: false
                }
              ],
              yAxes: [
                {
                  display: false
                }
              ]
            },
            elements: {
              line: {
                borderWidth: 2
              },
              point: {
                radius: 0,
                hitRadius: 10,
                hoverRadius: 4
              }
            }
          }
        }); // eslint-disable-next-line no-unused-vars

        let usersData = [<?php for ($i = 1; $i < 13; $i++) {
    if (isset($usersPerMonth[$i])) {
        echo $usersPerMonth[$i] . ', ';
    } else {
        echo '0, ';
    }
}?>]
        var cardChart4 = new Chart($('#card-users'), {
          type: 'bar',
          data: {
            labels: [ 'Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro' ],
            datasets: [
              {
                label: 'Usuários cadastrados',
                backgroundColor: 'rgba(255,255,255,.2)',
                borderColor: 'rgba(255,255,255,.55)',
                data: usersData,
              }
            ]
          },
          options: {
            maintainAspectRatio: false,
            legend: {
              display: false
            },
            scales: {
              xAxes: [
                {
                  display: false,
                  barPercentage: 0.6
                }
              ],
              yAxes: [
                {
                  display: false
                }
              ]
            }
          }
        }); // eslint-disable-next-line no-unused-vars


      })
    })(jQuery)
  </script>
@endpush
