<div class="row">
  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-primary">
      <div class="card-body pb-0">
        <div class="btn-group float-right">
          <i class="icon-layers"></i>
        </div>
        <div class="text-value">{{$occurrences}}</div>
        <div>Casos cadastrados</div>
      </div>
      <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
        <canvas class="chart" id="occurrences-chart" height="70"></canvas>
      </div>
    </div>
  </div>
  <!-- /.col-->
  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-success">
      <div class="card-body pb-0">
        <div class="btn-group float-right">
          <i class="icon-trophy"></i>
        </div>
        <div class="text-value">{{$occurrencesClosed}}</div>
        <div>Casos Encerrados</div>
      </div>
      <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
        <canvas class="chart" id="closed-chart" height="70"></canvas>
      </div>
    </div>
  </div>
  <!-- /.col-->
  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-info">
      <div class="card-body pb-0">
        <div class="btn-group float-right">
          <i class="icon-directions"></i>
        </div>
        <div class="text-value">{{$situationsCount}}</div>
        <div>Status Ativos</div>
      </div>
      <div class="chart-wrapper mt-3" style="height:70px;">
        <canvas class="chart" id="situations-chart" height="70"></canvas>
      </div>
    </div>
  </div>
  <!-- /.col-->
  <div class="col-sm-6 col-lg-3">
    <div class="card text-white bg-danger">
      <div class="card-body pb-0">
        <div class="btn-group float-right">
          <i class="icon-people"></i>
        </div>
        <div class="text-value">{{$usersCount}}</div>
        <div>Usuários cadastrados</div>
      </div>
      <div class="chart-wrapper mt-3 mx-3" style="height:70px;">
        <canvas class="chart" id="card-users" height="70"></canvas>
      </div>
    </div>
  </div>
  <!-- /.col-->
</div>
<!-- /.row-->
<!-- .row -->
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">Casos em aberto</div>
      <div class="card-body">
        <div class="row">
        @foreach($occurrencesPanel as $occurrence)
          <div class="col-6 col-lg-3">
            <div class="card">
              <div class="card-body p-0 d-flex align-items-center">
                <i class="icon-layers bg-{{substr($occurrence->situation->color,1,6)}} p-4 font-2x1 mr-3"></i>
                <div>
                  <div class="text-value-sm text-{{substr($occurrence->situation->color,1,6)}}">{{$occurrence->total}}</div>
                  <div class="text-muted text-uppercase font-weight-bold small">{{$occurrence->situation->description}}</div>
                </div>
              </div>
              <div class="card-footer px-3 py-2">
                <a href="{{route('occurrences.index', ['situation' => $occurrence->situation->id])}}" class="btn-block text-muted d-flex justify-content-between align-items-center">
                  <span class="small font-weight-bold">Ver mais</span>
                  <i class="fa fa-angle-right"></i>
                </a>
              </div>
            </div>
          </div>
        @endforeach
        </div>
      </div>
    </div>
  </div>

</div>
<!-- /.row -->
