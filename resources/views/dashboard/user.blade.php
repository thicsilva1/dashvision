<!-- /.row-->
<div class="row">
  <div class="col-md-12">
    <div class="card">
      <div class="card-header">Casos em aberto</div>
      <div class="card-body">
        <div class="row">
        @foreach($occurrencesPanel as $occurrence)
          <div class="col-6 col-lg-3">
            <div class="card">
              <div class="card-body p-0 d-flex align-items-center">
                <i class="icon-layers bg-{{substr($occurrence->situation->color,1,6)}} p-4 font-2x1 mr-3"></i>
                <div>
                  <div class="text-value-sm text-{{substr($occurrence->situation->color,1,6)}}">{{$occurrence->total}}</div>
                  <div class="text-muted text-uppercase font-weight-bold small">{{$occurrence->situation->description}}</div>
                </div>
              </div>
              <div class="card-footer px-3 py-2">
                <a href="{{route('occurrences.index', ['situation' => $occurrence->situation->id])}}" class="btn-block text-muted d-flex justify-content-between align-items-center">
                  <span class="small font-weight-bold">Ver mais</span>
                  <i class="fa fa-angle-right"></i>
                </a>
              </div>
            </div>
          </div>
        @endforeach
        </div>
      </div>
    </div>
  </div>

</div>
<!-- /.row -->
