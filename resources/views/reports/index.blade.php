@extends('layouts.app')

@section('content')
<main class="main">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="{{route('home')}}">Home</a></li>
    <li class="breadcrumb-item active">Relatório</li>
  </ol>
  <div class="container-fluid">
    <div class="ui-view">
      <div class="animated fadeIn">
        <div class="row">
          <div class="col-md-12">
            <form action="" class="form-horizontal">
              <div class="form-group row">
                <div class="col">
                  <input type="text" name="start_date" id="start_date" class="form-control date" placeholder="Data Início">
                </div>
                <div class="col">
                  <input type="text" name="end_date" id="end_date" class="form-control date" placeholder="Data Fim">
                </div>
                <div class="col">
                  <select name="to_user_id" id="to_user_id" class="form-control select2">
                    <option value="">Selecione Usuário</option>
                    @foreach($users as $user)
                    <option value="{{$user->id}}">{{$user->name}}</option>
                    @endforeach
                  </select>
                  </select>
                </div>
                <div class="col">
                  <select name="situation_id" id="situation_id" class="form-control select2">
                    <option value="">Selecione Status</option>
                    @foreach($situations as $situation)
                    <option value="{{$situation->id}}">{{$situation->description}}</option>
                    @endforeach
                  </select>
                  </select>
                </div>
                <div class="col">
                  <button class="btn btn-primary"><i class="fa fa-search"></i> Filtrar</button>
                </div>
              </div>
            </form>
          </div>
        </div>
        @if(isset($occurrences) && !empty($occurrences))

        <div class="row">
          <div class="col-md-12">
            <div class="card">
              <div class="card-header">
                Resultado da busca
                <a href="#" class="btn btn-sm btn-primary float-right mr-1 d-print-none" onclick="window.open('{{ route('reports.print.results', ['start_date'=>request()->start_date, 'end_date'=>request()->end_date, 'from_user_id'=>request()->from_user_id, 'to_user_id'=>request()->to_user_id, 'situation_id' => request()->situation_id]) }}','popUpWindow','height=500,width=700,left=500,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');"><i class="fa fa-print"></i> Imprimir</a>
              </div>
              <div class="card-body">
                <div class="table-responsive">
                  <table class="table datatable">
                    <thead>
                      <tr>
                        <td>Nome</td>
                        <td>Mensagem</td>
                        <td>Email</td>
                        <td>Telefone</td>
                        <td>Status</td>
                        <td>Criado em</td>
                        <td>Ações</td>
                      </tr>
                    </thead>
                    <tbody>
                      @foreach($occurrences as $occurrence)
                      <tr>
                        <td>{{$occurrence->name}}</td>
                        <td>{{substr_replace($occurrence->message, '...', 50)}}</td>
                        <td>{{$occurrence->email}}</td>
                        <td>{{$occurrence->phone}}</td>
                        <td><span class="badge badge-{{substr($occurrence->situation->color,1,6)}}">{{$occurrence->situation->description}}</span></td>
                        <td>{{$occurrence->created_at->format('d/m/Y H:i')}}</td>
                        <td>
                          <div class="btn-group btn-group-sm">
                            <a href="#" class="btn btn-primary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Imprimir" onclick="window.open('{{ route('occurrences.print', $occurrence) }}','popUpWindow','height=500,width=700,left=500,top=100,resizable=yes,scrollbars=yes,toolbar=yes,menubar=no,location=no,directories=no, status=yes');"><i class="fa fa-print"></i></a>
                            <a href="{{route('occurrences.show', $occurrence)}}" class="btn btn-info" data-toggle="tooltip" data-placement="top" title="" data-original-title="Detalhar"><i class="fa fa-newspaper-o"></i></a>
                          </div>
                        </td>
                      </tr>
                      @endforeach
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
          </div>
        </div>
        @endif
      </div>
    </div>
  </div>
</main>
@stop

@push('scripts')
  <script src="/vendors/inputmask/js/jquery.inputmask.bundle.js"></script>
  <script>
    (function(){
      $('.date').inputmask({"mask": ['99/99/9999'], "keepstatic":true});
    })(jQuery)
  </script>
@endpush
