<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */

Route::get('/', function () {
    return redirect('home');
});

Auth::routes();

Route::group(['middleware' => 'auth'], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/notifications', 'HomeController@notifications');
    Route::group(['middleware' => 'can:occurrences', 'prefix' => 'occurrences', 'as' => 'occurrences.'], function () {
        Route::get('/', 'OccurrenceController@index')->name('index')->middleware('can:occurrences_list');
        Route::get('/create', 'OccurrenceController@create')->name('create')->middleware('can:occurrences_add');
        Route::post('/', 'OccurrenceController@store')->name('store')->middleware('can:occurrences_add');
        Route::get('/{occurrence}/edit', 'OccurrenceController@edit')->name('edit')->middleware('can:occurrences_edit');
        Route::put('/{occurrence}', 'OccurrenceController@update')->name('update')->middleware('can:occurrences_edit');
        Route::delete('/{occurrence}', 'OccurrenceController@destroy')->name('destroy')->middleware('can:occurrences_destroy');
        Route::get('/print/{occurrence}', 'ReportController@printOccurrence')->name('print')->middleware('can:occurrences_print');

    });
    Route::group(['middleware' => 'can:situations', 'prefix' => 'situations', 'as' => 'situations.'], function () {
        Route::get('/', 'SituationController@index')->name('index')->middleware('can:situations_list');
        Route::get('/create', 'SituationController@create')->name('create')->middleware('can:situations_add');
        Route::post('/', 'SituationController@store')->name('store')->middleware('can:situations_add');
        Route::get('/{situation}/edit', 'SituationController@edit')->name('edit')->middleware('can:situations_edit');
        Route::put('/{situation}', 'SituationController@update')->name('update')->middleware('can:situations_edit');
        Route::delete('/{situation}', 'SituationController@destroy')->name('destroy')->middleware('can:situations_destroy');
    });

    Route::group(['middleware' => 'can:occurrences'], function () {
        Route::group(['prefix' => 'occurrences/{occurrence}', 'as' => 'occurrences.'], function () {
            Route::get('/', function () {
                return redirect()->route('occurrences.index');
            });
            Route::post('/close', 'OccurrenceController@closeOccurrence')->name('close')->middleware('can:occurrences_close');
            Route::get('/details', 'OccurrenceController@show')->name('show');
            Route::post('/situation', 'OccurrenceController@updateSituation')->name('update.situation');
            Route::post('/message', 'MessageController@store')->name('message.store');
            Route::delete('/message/{message}', 'MessageController@destroy')->name('message.destroy');
            Route::post('/attachment', 'AttachmentController@store')->name('attachment.store');
            Route::delete('/attachment/{attachment}', 'AttachmentController@destroy')->name('attachment.destroy');
            Route::get('/attachment/{attachment}', 'AttachmentController@download')->name('attachment.download');

        });
    });

    Route::group(['middleware' => 'can:reports'], function () {
        Route::get('/reports', 'ReportController@index')->name('reports.index');
        Route::get('/results', 'ReportController@printVariousOccurrence')->name('reports.print.results');
    });

    Route::group(['middleware' => 'can:users', 'prefix' => 'users', 'as' => 'users.'], function () {
        Route::get('/', 'UserController@index')->name('index')->middleware('can:users_list');
        Route::get('/create', 'UserController@create')->name('create')->middleware('can:users_add');
        Route::post('/', 'UserController@store')->name('store')->middleware('can:users_add');
        Route::get('/{user}/edit', 'UserController@edit')->name('edit')->middleware('can:users_edit');
        Route::put('/{user}', 'UserController@update')->name('update')->middleware('can:users_edit');
        Route::delete('/{user}', 'UserController@destroy')->name('destroy')->middleware('can:users_destroy');
    });

    Route::group(['middleware' => 'can:roles', 'prefix' => 'roles', 'as' => 'roles.'], function () {
        Route::get('/', 'RoleController@index')->name('index')->middleware('can:roles_list');
        Route::get('/create', 'RoleController@create')->name('create')->middleware('can:roles_add');
        Route::post('/', 'RoleController@store')->name('store')->middleware('can:roles_add');
        Route::get('/{role}/edit', 'RoleController@edit')->name('edit')->middleware('can:roles_edit');
        Route::put('/{role}', 'RoleController@update')->name('update')->middleware('can:roles_edit');
        Route::delete('/{role}', 'RoleController@destroy')->name('destroy')->middleware('can:roles_destroy');
    });

    Route::get('/profile', 'ProfileController@edit')->name('profile.edit');
    Route::put('/profile', 'ProfileController@update')->name('profile.update');
    Route::post('/profile/password', 'ProfileController@updatePassword')->name('profile.password');
});
